# README #

### What is this repository for? ###

This project is a very simple and lightweight C code to create a randomiser for students who want to test themselves
against a multitude of concepts for a test. Initially created for a Computer Graphics test.

### How do I get set up? ###

For now this is a single-file only project in C. Just gcc it!

Execution: ./<executable> arg1 arg2

This only works for the basic version with 2 lists of things. A different number will require arg# parameters

### Contribution guidelines ###

Ok, this is a REALLY SMALL and DUMB project. Just setting it up because I think there are people who would like
to have this kind of code. There is still a lot to do!

TODO:
Comments. Yeah, it is a requirement of good code and I haven't done yet for lack of time. As I said, it started as a simple code during a boring time.
Figuring out if there is a better way to work with the lists. Probably.
Mega-super-duper version: text inputs. Again, simple to do but not now in the beginning.

### Who do I talk to? ###

Here am I! Feel free to contact if you have anything to say different than "this code sucks". Yes, I know it.
ghapereira@gmail.com