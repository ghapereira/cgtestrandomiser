#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CONCEPTS 1
#define ALGORITHMS 2
#define NUMBER_OF_TOPICS 2
#define ERROR_EXIT 1

static const char * concepts[] = {
    "geometric transformations", "2D uniform/nonuniform scaling + matrix",
    "2D rotation + matrix", "2D translation + matrix", 
    "matrix representation of a 2D transformation",
    "properties of linear transformations", "homogeneous coordinates",
    "properties of affine transformations", "matrix composition",
    "matrices of 3D reverse transformations", "parametric line equation",
    "point to plane test", "primitives", "geometric primitives",
    "output primitives", "criteria of a line", "incremental algorithm",
    "decision variable"
};

static const char * algorithms[] = {
    "Window-viewport transformation",
    "Cohen-Sutherland Line clipping (pros and cons)",
    "Cyrus-Beck Line clipping",
    "Sutherland-Hodgman Clipping",
    "Line clipping for non-convex polygon",
    "DDA",
    "Midpoint Line (also issues)",
    "Midpoint circle"
};

void generateIssueList(short desiredItems, short list,
                       const char * nameOfList){
    short * listOfItems;
    short listPointer = 0;
    short randomNumber;
    short i = 0;
    short exists = 0;

    short numberOfItems;

    switch(list){
        case CONCEPTS:
            numberOfItems = (short)(sizeof(concepts)/sizeof(char *));
        break;
        case ALGORITHMS:
            numberOfItems = (short)(sizeof(algorithms)/sizeof(char *));
        break;
        default:
            printf("Not a valid list\n");
            exit(ERROR_EXIT);
    }
    
    if(desiredItems > numberOfItems){
        printf("%s:\t", nameOfList);
        printf("More items than existing on list!\n");
        return;
    }

    listOfItems = (short *)malloc(sizeof(short) * desiredItems);

    srand(time(NULL));

    while(listPointer < desiredItems){                
        randomNumber = (short)(rand() % numberOfItems);
        
        if(listPointer > 0){        
            exists = 0;
            for(i = 0; i < listPointer; i++){
                if(listOfItems[i] == randomNumber) exists = 1;
            }     

            if(! exists) listOfItems[listPointer++] = randomNumber;             
        }
        else listOfItems[listPointer++] = randomNumber;
    }

    printf("%s:\n", nameOfList);

    switch(list){
        case CONCEPTS:
            for(i = 0; i < desiredItems; i++){
                printf("\t%d:\t%s\n", i + 1, concepts[listOfItems[i]]);
            }
        break;
        case ALGORITHMS:
            for(i = 0; i < desiredItems; i++){
                printf("\t%d:\t%s\n", i + 1, algorithms[listOfItems[i]]);
            }
        break;
        default:
            printf("Non-existing list\n");
    }

    free(listOfItems);  
}

int main(int argc, char * argv[]){
    if(argc != NUMBER_OF_TOPICS + 1) exit(ERROR_EXIT);

    short desiredConcepts = (short)atoi(argv[1]);  
    short desiredAlgorithms = (short)atoi(argv[2]);

    generateIssueList(desiredConcepts, (short)CONCEPTS, "Concepts");
    generateIssueList(desiredAlgorithms, (short)ALGORITHMS, "Algorithms");

    return 0;
}